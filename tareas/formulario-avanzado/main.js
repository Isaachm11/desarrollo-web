let nombresFieldset = ['primerFieldset', 'segundoFieldset', 'tercerFieldset', 'cuartoFieldset', 'quintoFieldset'];
let elementToHide,
    elementToShow;

['segundoFieldset', 'tercerFieldset', 'cuartoFieldset', 'quintoFieldset'].forEach(id => {
    elementToHide = document.getElementById(id);
    elementToHide.style.display = 'none';
});

function siguienteFieldset(boton) {
    elementToHide = document.getElementById(boton.id).parentElement.parentElement;
    elementToHide.style.display = 'none';
    elementToShow = elementToHide.nextElementSibling;
    elementToShow.style.display = 'flex';
}

function anteriorFieldset(boton) {
    elementToHide = document.getElementById(boton.id).parentElement.parentElement;
    elementToHide.style.display = 'none';
    elementToShow = elementToHide.previousElementSibling;
    elementToShow.style.display = 'flex';
}

function validarCURP(curp) {
    if (curp.value.length === 18) {
        curp.classList.remove("not-valid")
    } else {
        curp.classList.add("not-valid")
    }
}

function validarTelEmergencia(telEmergencia) {
    let regex = /^\d{10}$/;

    if (telEmergencia.value.match(regex)) {
        telEmergencia.classList.remove("not-valid")
    } else {
        telEmergencia.classList.add("not-valid")
    }
}

async function prellenaDireccion(field) {
    let cp = field.value
    let token = 'cf67db0d-be5d-4523-9c29-81756cc5c5aa' // get token from website
    try {
        const res = await fetch(
            `https://api-sepomex.hckdrk.mx/query/info_cp/${cp}?=simplified&token=${token}`
        )
        const data = await res.json()
        if (data.error !== false) {
            const {
                response
            } = data[0]
            document.getElementById("colonia").value = response.asentamiento;
            document.getElementById("municipio").value = response.municipio;
            document.getElementById("estado").value = response.estado;
            document.getElementById("pais").value = response.pais;

        }
    } catch (error) {
        console.log(error)
    }
}

function submitHandle(form) { 
    console.log(form);
    return false
}