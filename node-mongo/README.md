# 🍳 Frit-Angas
Es un programa que ayuda con el inventario de cualquier empresa. Incluye el manejo de artículos los cuales tienen un proveedor. 

## 📄 Requerimientos Fundamentales: 
- [Node.js](###Node)
- [Express](###Express)
- [MongoDB](###MongoDB)
- [JavaScript](###JavaScript)
- [jQuery](###jQuery)
- [CSS](###CSS)
- [Bootstrap](###Bootstrap)

### Node
Node.js es un entorno en tiempo de ejecución multiplataforma, de código abierto, para la capa del servidor (pero no limitándose a ello) basado en el lenguaje de programación ECMAScript, asíncrono, con I/O de datos en una arquitectura orientada a eventos y basado en el motor V8 de Google [1]. Node, al ser muy popular en el mundo del desarrollo web, tiene librerías que ayudan a que el desarrollo sea más fácil. En el caso de este proyecto una de las librerías que utilizamos es *mongodb (^3.6.6)* para la conexión entre el backend y la base de datos.

### Express
Es un framework de aplicaciones web Node.js mínima y flexible que proporciona un conjunto sólido de características para las aplicaciones web y móviles. Con Express, pudimos obtener la arquitectura de microservicios mediante sus métodos de utilidad de HTTP. Con express, crear una API es rápido y fácil. [2]

### MongoDB 
En este proyecto se utilizó la base de datos de MongoDB la cual es una base de datos basada en documentos, lo que facilita la operación de CRUD dentro de la aplicación y lo hace de una manera más efectiva que en otras bases de datos. 

Para este proyecto se tiene una instancia de base de datos con dos colecciones, una para los proveedores y otra para los artículos. Para la conexión a MongoDB se utilizo una URL apuntada a localhost de la siguiente manera.

```javascript
var MongoClient = require("mongodb").MongoClient,
  ObjectID = require("mongodb").ObjectID;
```

```javascript
const mongoUrl =
  "mongodb://localhost:27017/?readPreference=primary&appname=MongoDB%20Compass&ssl=false";
```
Posteriormente nos conectamos  on el MongoClient a esa url de la siguiente manera.

```javascript
  MongoClient.connect(mongoUrl, {
      useUnifiedTopology: true,
    })
```

### JavaScript
Es un lenguaje de secuencias de comandos que te permite crear contenido de actualización dinámica, controlar multimedia, animar imágenes y prácticamente todo lo demás. En este proyecto utilizamos JavaScript para crear script que nos ayuden a tener la información que se muestra al usuario actualizada, para llamar a las APIs con el objetivo de agragar y actualizar infomación.

### jQuery
Es una librería de JavaScript que importamos al proyecto que ayuda con la manipulación de eventos, del HTML, animaciones, [Ajax](https://api.jquery.com/jquery.ajax/) (el cual utilizamos para las llamadas al backend o APIs) y muchas más características que hacen el escribir código de JS más fácil.

### CSS
Es el lenguaje de estilos que se utiliza para darle una mejor presentación a las páginas de HTML. Para el proyecto lo utilizamos con el objetivo de presentar la información de una manerá más profesional.

### Bootstrap
Es una librería de diseños ya establecidos que ayuda a desarrollar páginas web de manera más rápida. Contiene plantillas de diseño con tipografía, formularios, botones, cuadros, menús de navegación y otros elementos de diseño basado en HTML y CSS, así como extensiones de JavaScript adicionales.

## ⏳ Instalación
Debes de tener previamente instalado git

### Base de datos
Para crear tu base de datos en localhost debes seguir las instrucciones del siguiente tutorial: [Crea una base de datos en MongoDB](https://www.mongodb.com/basics/create-database.). La base de datos debe de cumplir con las siguientes restricciones:
- Estar corriendo en local
- La instancia de la base de datos se debe llamar 'inventario'
- Debe de tener dos colecciones llamadas:
  - articulos
  - proveedores
- Debe de estar corriendo en el puerto 27027 (default)

### Backend
1. Abrir una terminal o línea de comandos y descargar el siguiente repositorio
```bash
git clone git@gitlab.com:Isaachm11/desarrollo-web.git
```
2. Moverse a la carpeta de backend
```bash
cd path/to/desarrollo-web/node-mongo/backend
```
3. Eliminar el archivo package-lock.json
4. Eliminar la carpeta node_modules
5. Instalar las dependencias
```bash
npm install
```

### Fontend
Debes de tener python. Mínimo una versión 3.7.
Abrir otra termial o línea de comandos
1. Correr el comando
```bash
python -m http.server
```


## 🚀 Correr el programa
1. Corra el backend y el frontend como se mencionó anteriormente.
1. Ingresar en su navegador de preferencia la ip: 127.0.0.1:8000.
2. Agregue un proveedor
3. Agregue un artículo
4. Agregue más proveedores y artículos

# Archivos

### Frontend
  - css
    - main.css
  - js
    - main.js
  - templates
    - articulo_edit_form.html (Editar artículos)
    - articulo_form.html (Agregar artículos)
    - index.html (Ver artículos)
    - proveedor_edit_form.html (Editar proveedores)
    - proveedor_form.html (Agregar proveedores)
    - proveedores.html (Ver proveedores)

### Backend
- node_modules
- index.js
- package-lock.json
- package.json
- router.js

## Fuentes
- [Node][1]
- [Express][2]
- [Qué es Javascript][3]


<!-- Sources -->
[1]: https://es.wikipedia.org/wiki/Node.js
[2]: https://expressjs.com/es/
[3]: https://developer.mozilla.org/es/docs/Learn/JavaScript/First_steps/What_is_JavaScript